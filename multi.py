import tornado
import tornado.websocket
import tornado.ioloop
import tornado.web


class WSHandler(tornado.websocket.WebSocketHandler):
    def initialize(self, listeners):
        self.listeners = listeners

    def open(self):
        print("New connection")
        self.listeners.append(self)

    def on_message(self, message):
        print("message received {}".format(message))
        for listener in self.listeners:
            listener.write_message("Someone said {}".format(message.upper()))

    def on_close(self):
        print("Connection closed")

    def check_origin(self, _):
        return True


if __name__ == "__main__":
    listeners = []

    app = tornado.web.Application([
        (r'/ws', WSHandler, {'listeners': listeners}),
    ])
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(8888)

    tornado.ioloop.IOLoop.instance().start()
