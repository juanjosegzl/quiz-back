import tornado
import tornado.websocket
import tornado.ioloop
import tornado.web

class WSHandler(tornado.websocket.WebSocketHandler):
    def open(self):
        print("New connection")

    def on_message(self, message):
        print("message received {}".format(message))
        self.write_message("Send {}".format(message.upper()))

    def on_close(self):
        print("Connection closed")

    def check_origin(self, _):
        return True


if __name__ == "__main__":
    app = tornado.web.Application([
        (r'/ws', WSHandler),
    ])
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(8888)

    tornado.ioloop.IOLoop.instance().start()
