# -*- coding: utf-8 -*-
import tornado
import tornado.websocket
import tornado.ioloop
import tornado.web
import socket
import json
import string
from collections import defaultdict
import random
import os

ENV = os.environ.get('ENV', 'dev')

class ListenersPool:
    def __init__(self):
        self.listeners = {}

    def add_listener(self, conn):
        self.listeners[conn] = conn

    def remove_listener(self, conn):
        del self.listeners[conn]

class Quiz:
    def __init__(self):
        self.id_ = self.create_game_id()
        self.participants = dict()
        self.participant_host = None
        self.scores = dict()
        self.winner = None

        self.questions = [
            '¿Quién es el creador de Python?',
            '¿Qué significa PEP?',
            '¿Cuál de estas estructuras necesita un import explícito?',
            '¿Cómo se obtiene la longitud de una cadena?',
            '¿Cuál de estos import es incorrecto?',

            '¿Qué es usado para definir un bloque de código?',
            '¿Qué tipo de dato tiene n? n = "4"',
            '¿Qué tipo de dato es a? a = (1, 2)',
            '¿Qué función se utiliza para tomar entradas del teclado?',
            '¿Qué librería tiene un uso similar a curl o wget?',

            '¿Qué método es usado para agregar un valor a una lista?',
            '¿Cuál palabra reservada rompe la iteración actual en un ciclo?',
            '¿Qué palabra reservada no tiene relación con excepciones?',
            '¿Qué función se usa para abrir un archivo?',
            '¿Qué palabra reservada se usa para declarar una función?',

            '¿Cuál de las siguientes palabras reservadas es válido como único cuerpo en una función?',
            '¿Qué archivo se coloca en un directorio para marcarlo como paquete?',
            '¿Cuál de estas estructuras es hasheable?',
            '¿Cuál de estos no es una estructura de control en python?',
            '¿Qué convención se utiliza para marcar métodos como mágicos?',
        ]

        self.answers = [
            ('Linus Torvalds', 'Rasmus Lerdorf', 'Guido van Rossum', 'Larry Wall'),
            ('Program Engine Python', 'Python Enhacement Proposal', 'Python Enhanced Printer', 'Pretty Editor Python'),
            ('queue', 'list', 'dict', 'tuple'),
            ('len(str)', 'str.length', 'length(str)', 'size(str)'),
            ('import requests', 'from marshmallow import Schema', 'from pygame import *', 'import * from pycurl'),

            ('llaves', 'paréntesis', 'indentación', 'espacios'),
            ('cadena', 'entero', 'flotante', 'tupla'),
            ('cadena', 'lista', 'tupla', 'diccionario'),
            ('scanf', 'input', 'cin', 'entry'),
            ('requests', 'marshmallow', 'pygame', 'numpy'),

            ('delete', 'concat', 'extend', 'append'),
            ('pass', 'next', 'stop', 'break'),
            ('try', 'raise', 'except', 'catch'),
            ('openfile', 'open', 'fopen', 'open_file'),
            ('def', 'function', 'macro', 'func'),

            ('break', 'continue', 'pass', 'body'),
            ('init.py', '__init__.py', 'setup.py', 'pkg.module'),
            ('list', 'dict', 'set', 'tuple'),
            ('while', 'for', 'if', 'do ... while'),
            ('_magic', '@magic', '__magic__', ':magic'),
        ]

        self.correct = [
            2, 1, 0, 0, 3,
            2, 0, 2, 1, 0,
            3, 3, 3, 1, 0,
            2, 1, 3, 3, 2,
        ]

    def join(self, nickname, wshandler):
        if len(self.participants) < 4:
            if nickname in self.participants:
                nickname += '_'
            self.participants[nickname] = wshandler
            self.scores[nickname] = 0
            return nickname
        return False

    def host(self, wshandler):
        self.participant_host = wshandler

    def answer(self, question_id, answer_id, user_id):
        if self.correct[question_id] == answer_id:
            self.scores[user_id] += 1
            if self.scores[user_id] == 5:
                self.winner = user_id

            del self.questions[question_id]
            del self.answers[question_id]
            del self.correct[question_id]

            return True

        return False

    def get_question_id(self):
        return random.choice(range(len(self.questions)))

    @staticmethod
    def create_game_id():
        return ''.join([random.choice(string.ascii_uppercase) for _ in range(4)])


def create_game(_, wshandler):

    quiz = Quiz()
    quiz.host(wshandler)
    wshandler.quizes[quiz.id_] = quiz

    return {
        'success': True,
        'event': 'hosting',
        'quiz': quiz.id_,
    }

def join_game(message, wshandler):
    EVENT_NAME = 'joined'
    quiz_id = message.get('quiz', None)

    quiz = wshandler.get_quiz(quiz_id)
    if not quiz:
        return {
            'success': False,
            'event': EVENT_NAME,
            'message': 'No valid quiz id'
        }

    nickname = message.get('nick', Quiz.create_game_id())
    if not nickname:
        nickname = Quiz.create_game_id()
    user_id = quiz.join(nickname, wshandler)
    if not user_id:
        return {
            'success': False,
            'event': EVENT_NAME,
            'message': 'No room for more participants'
        }

    wshandler.write_all(quiz_id, {
        'success': True,
        'event': 'joined',
        'user': user_id,
        'quiz': quiz_id,
        'participants': len(quiz.participants),
    })
    return None

def start_game(message, wshandler):
    EVENT_NAME = 'started'
    quiz_id = message.get('quiz', None)

    quiz = wshandler.get_quiz(quiz_id)
    if not quiz:
        return {
            'success': False,
            'event': EVENT_NAME,
            'message': 'No valid quiz id'
        }

    wshandler.write_all(quiz_id, {
        'success': True,
        'event': 'started',
        'quiz': quiz_id,
        'scores': quiz.scores,
    })

    return None

def get_question(message, wshandler):
    EVENT_NAME = 'new_question'
    quiz_id = message.get('quiz', None)
    quiz = wshandler.get_quiz(quiz_id)

    if not quiz:
        return {
            'success': False,
            'event': EVENT_NAME,
            'message': 'No valid quiz id'
        }

    question_id = quiz.get_question_id()

    wshandler.write_all(quiz_id, {
        'success': True,
        'event': 'new_question',
        'quiz': quiz_id,
        'question': question_id,
        'question_text': quiz.questions[question_id],
        'answers': quiz.answers[question_id]
    })

    return None


def receive_answer(message, wshandler):
    EVENT_NAME = 'answered'
    quiz_id = message.get('quiz', None)
    quiz = wshandler.get_quiz(quiz_id)

    if not quiz:
        return {
            'success': False,
            'event': EVENT_NAME,
            'message': 'No valid quiz id'
        }

    question_id = message.get('question', None)
    answer_id = message.get('answer', None)
    user_id = message.get('user', None)
    if question_id is None or answer_id is None or user_id is None:
        return {
            'success': False,
            'event': EVENT_NAME,
            'message': 'Invalid payload {}'.format(message)
        }
    correct = quiz.answer(question_id, answer_id, user_id)

    wshandler.write_all(quiz_id, {
        'success': True,
        'event': EVENT_NAME,
        'quiz': quiz_id,
        'scores': quiz.scores,
        'correct': correct,
        'user': user_id,
    })

    if quiz.winner:
        declare_winner(message, wshandler)
        return None

    if correct:
        get_question(message, wshandler)

    return None

def declare_winner(message, wshandler):
    EVENT_NAME = 'winner'
    quiz_id = message.get('quiz', None)
    quiz = wshandler.get_quiz(quiz_id)

    if not quiz:
        return {
            'success': False,
            'event': EVENT_NAME,
            'message': 'No valid quiz id'
        }

    wshandler.write_all(quiz_id, {
        'success': True,
        'event': EVENT_NAME,
        'quiz': quiz_id,
        'scores': quiz.scores,
        'winner': quiz.winner
    })

    return None


def unknown_event(message, _):
    return {
        'success': False,
        'event': 'error',
        'message': 'Received unknown event: {}'.format(message.get('event'))
    }

class WSHandler(tornado.websocket.WebSocketHandler):
    def initialize(self, listeners_pool, quizes):
        self.listeners_pool = listeners_pool
        self.quizes = quizes

        self.event_handlers = {
            'create': create_game,
            'join': join_game,
            'unknown': unknown_event,
            'start': start_game,
            'get_question': get_question,
            'answer': receive_answer,
        }

    def get_quiz(self, quiz_id):
        return self.quizes.get(quiz_id.upper(), None)

    def write_all(self, quiz_id, message):
        quiz = self.quizes.get(quiz_id, None)
        if quiz:
            quiz.participant_host.write_message(message)
            for p in quiz.participants:
                quiz.participants[p].write_message(message)

    def open(self):
        print("New connection")

    def on_message(self, message):
        print "message received {}".format(message)
        message = json.loads(message)
        event = message.get('event', 'unknown')

        try:
            handler = self.event_handlers[event]
        except KeyError:
            print("Unknown handler")
            handler = unknown_handler
        response = handler(message, self)
        if response:
            self.write_message(response)

    def on_close(self):
        print("Connection closed")

    def check_origin(self, _):
        return True


if __name__ == "__main__":
    listeners_pool = ListenersPool()
    quizes = defaultdict(Quiz)

    app = tornado.web.Application([
        (r'/ws', WSHandler, {'listeners_pool': listeners_pool, 'quizes': quizes}),
    ])
    ssl_options = {}
    if ENV == 'prod':
        ssl_options={
            "certfile": "certs/server.crt",
            "keyfile": "certs/server.key",
        }

    http_server =tornado.httpserver.HTTPServer(app, ssl_options=ssl_options)
    http_server.listen(8888)

    myIP = socket.gethostbyname(socket.gethostname())
    print "Websocket Server Started {}".format(myIP)
    tornado.ioloop.IOLoop.instance().start()
